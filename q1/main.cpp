#include <iostream>
#include <thread>
#include "MessagesSender.h"
int main(void)
{
	MessagesSender* menu = new MessagesSender();
	thread msgReader(&MessagesSender::readMessages, menu);
	thread msgSender(&MessagesSender::sendMessages, menu);
	menu->useMenu();
	//this_thread::sleep_for(chrono::seconds(1));
	msgReader.detach();
	msgSender.detach();
	
	return 0;
}