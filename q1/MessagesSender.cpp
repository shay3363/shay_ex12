#include "MessagesSender.h"
MessagesSender::MessagesSender()
{
	this->_users = vector<string>();
	this->_messages = queue<string>();
}

MessagesSender::~MessagesSender()
{
}

void MessagesSender::printMenu()
{
	this->_vectorLock.lock();
	cout << "1. Signin\n2. Signout\n3. Connected Users\n4. exit\n";
	this->_vectorLock.unlock();
}

bool MessagesSender::isSigned(string name)
{
	int i = 0;
	bool flag = false;
	this->_vectorLock.lock();
	for (i = 0; i < this->_users.size() && !flag; i++)
	{
		if (this->_users[i] == name)
		{
			flag = true;
		}
	}
	this->_vectorLock.unlock();
	return flag;
}

void MessagesSender::signIn(string name)
{
	this->_vectorLock.lock();
	this->_users.push_back(name);
	this->_vectorLock.unlock();
}

void MessagesSender::signOut(string name)
{
	this->_vectorLock.lock();
	this->_users.erase(find(this->_users.begin(), this->_users.end(), name));
	this->_vectorLock.unlock();
}

void MessagesSender::printConnected()
{
	int i = 0;
	this->_vectorLock.lock();
	for (i = 0; i < this->_users.size(); i++)
	{
		cout << this->_users[i] << " ";
	}
	this->_vectorLock.unlock();
	cout << endl;
}

void MessagesSender::useMenu()
{
	int choose = 0;
	string name = "";
	while (choose != 4)
	{
		this->printMenu();
		cin >> choose;
		switch (choose)
		{
		case 1:
			cout << "enter username" << endl;
			cin >> name;
			if (!this->isSigned(name))
			{
				this->signIn(name);
			}
			else
			{
				cout << "name already exist" << endl;
			}
			break;
		case 2:
			cout << "enter username" << endl;
			cin >> name;
			if (this->isSigned(name))
			{
				this->signOut(name);
			}
			else
			{
				cout << "name not exist" << endl;
			}
			break;
		case 3:
			this->printConnected();
			break;
		case 4:
			cout << "bye" << endl;
			break;
		default:
			cout << "wrong number" << endl;
			break;
		}
	}
}

void MessagesSender::readMessages()
{
	fstream dataFile;
	string line;
	while (true)
	{
		dataFile.open("data.txt");
		this->_queueLock.lock();
		while (getline(dataFile, line))
		{
			this->_messages.push(line);
		}
		this->_queueLock.unlock();
		dataFile.close();
		dataFile.open("data.txt" , ofstream::out | ofstream::trunc);
		dataFile.close();
		this_thread::sleep_for(chrono::seconds(60));
	}
}

void MessagesSender::sendMessages()
{
	int i = 0;
	fstream outputFile;
	while (true)
	{
		this->_queueLock.lock();
		this->_vectorLock.lock();
		outputFile.open("output.txt");
		while (!this->_messages.empty())
		{
			for (i = 0; i < this->_users.size(); i++)
			{
				outputFile << this->_users[i] << ": " << this->_messages.front() << endl;
			}
			this->_messages.pop();
		}
		outputFile.close();
		this->_queueLock.unlock();
		this->_vectorLock.unlock();
	}
}
