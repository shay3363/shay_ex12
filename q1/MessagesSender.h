#pragma once
#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <queue>
#include <thread>
#include <mutex>
using namespace std;
class MessagesSender
{
private:
	vector<string> _users;
	queue<string> _messages;
	mutex _queueLock;
	mutex _vectorLock;

	void printMenu();
	bool isSigned(string name);
	void signIn(string name);
	void signOut(string name);
	void printConnected();
public:
	MessagesSender();
	~MessagesSender();
	void useMenu();
	void readMessages();
	void sendMessages();
};